# Présentation générale


???+ important "Où et quand ? (*Cliquez ici pour cacher / montrer*)"

    * Nous nous retrouvons le **troisième samedi du mois**, de **9h à 12h**, 
      à la **Maison pour Tous d'Amancy** (près de la Roche-sur-Foron) :    
      **15/03/2025**, 19/04, 17/05, 21/06... 
      Voir les détails dans **[la rubrique dédiée](contact)**.
      

    * **Thèmes du moment** : installations GNU/Linux... (_Prévenez-nous à
      l'avance si possible, si vous avez d'autres attentes ou si vous désirez
      récupérer des  PC sous Linux à l'occasion de nos rencontres_), « outils
      libres collaboratifs / en ligne » (NextCloud, OnlyOffice...), idem pour
      les réseaux sociaux (Mastodon, PeerTube & Co.), etc.

??? info "C'est reparti 👍 !"
    Après un arrêt forcé de nos activités qui avaient démarré à la BASE du 
    Pays Rochois, nous avons eu la joie de relancer des rencontres mensuelles
    ([toutes les infos sont ici](contact)), grâce à la 
    municipalité d'Amancy qui nous met une belle salle à disposition.
    
    <center>![Image de Tux heureux](img/tux_heureux.png)</center>
    
    _Nous conservons pour l'instant le nom de ce site, en souvenir de
    nos débuts et parce que l'essentiel c'est de rester **libre, à la base** !_

    La première rencontre a été l'occasion d'échanges fort sympathiques 
    et plusieurs projets sont déjà lancés ou en voie de l'être !
    
    <center>![Photos de la première](img/premiere.jpg)</center>


## Pourquoi choisir les logiciels libres ?

> **Que peuvent m'apporter Linux et les autres logiciels libres**, en quoi 
serai-je concerné (à titre individuel, associatif, professionnel) ?

*Le respect de la vie privée, du libre arbitre, la sécurité, la luttre contre 
l'obsolescence programmée, la pérennité de vos outils, l'esprit d'entraide
et de partage, ça devrait vous intéresser...*

*Le plus simple, c'est de venir en discuter sur place !*

## De quoi s'agit-il ?

> Ok, ça paraît intéressant, mais **qu'est-ce qu'un logiciel libre** 
précisément ?

*Rien de très compliqué. Ce concept garantit des qualités essentielles (sans
logiciel libre, pas de garantie possible du respect de l'utilisateur et on se
retrouve souvent client captif du produit utilisé). 
Changer d'outil demande parfois un temps d'adaptation, mais le jeu en vaut 
la chandelle.*

*Nous avons réuni [quelques explications](what)
si ça vous intéresse. Et on trouve énormément d'informations sur le sujet
en cherchant un peu sur la toile.*

## Comment s'y mettre ? Ça vaut vraiment le coup ?

> J'ai peur de sauter le pas, je crains de galérer...

_**Tout le monde peut s'y mettre progressivement**, sans forcément « passer à 
100% à Linux » tout de suite. Le premier service que nous offrons est de
présenter les actions possibles, selon vos attentes et de vous permettre 
d'expérimenter sans risque l'utilisation et l'installation de logiciels libres,
sur place ou tranquillement chez vous ensuite._

_Après avoir fait le point sur vos pratiques, vos objectifs, votre motivation 
ou votre disponibilité, **nous vous accompagnons dans votre transition**._

_Et il y a [plein de bonnes raisons](why) de privilégier les logiciels libres, 
dont nous parlerons ensemble si vous venez nous rejoindre..._

!!! info "Note importante"
    Si vous voulez installer Linux sur votre ordinateur, nous vous proposons 
    de le faire ensemble et vous donnerons quelques 
    [conseils pour bien préparer](preinstall) cette étape. 
    
    Vous ne serez pas seul ensuite, notamment grâce à notre 
    [liste d'échanges par email](contact) à laquelle vous pouvez vous inscrire 
    librement.
