# Préparation d'une installation

[↰](/how)

!!! danger "Important"

    Installer un système d'exploitation n'est pas sans risque, même si nous 
    l'avons déjà réalisé de nombreuses fois avec succès. C'est pouquoi nous 
    vous demandons de **signer une [décharge](decharge)** de responsabilité 
    en cas de problème. 
    Et surtout, nous vous conseillons de **valider les points suivants** au 
    préalable, pour limiter les risques au maximum et gagner du temps le 
    jour J.

- [ ] **Sauvegarder ses données personnelles, sur un support externe** à 
l'ordinateur sur lequel se fera l'installation. C'est sans doute le point le 
plus important, car contrairement aux logiciels, si elles sont perdues, vous 
n'aurez aucune possilibité de les retrouver un jour.

- [ ] Essayer de **déterminer les touches à activer au démarrage pour accéder 
au BIOS** (ou UEFI). Elles sont parfois affichées (rapidement) lors du 
démarrage. Sinon, cela peut être indiqué dans la documentation, que vous 
pourrez retrouver sur internet. Vous pouvez aussi expérimenter, en redémarrant 
votre ordinateur (après un véritable arrêt, pas une simple mise en veille) puis 
en appuyant immédiatement après de manière répétée sur la touche supposée être 
la bonne, durant la phase de démrrage. C'est souvent ++f1++ ou ++f2++, ++f8++,
++f10++, ++esc++. Si vous ne voyez pas de quoi on parle ici, on cherchera 
ensemble le jour J.

      **À défaut**, il peut être suffisant de pouvoir **démarrer ponctuellement
sur une clé USB**. De même, si vous ne disposez pas de la documentation, testez
l'appui répété sur ++f12++ au démarrage pour voir si un menu spécifique vous
est proposé.

- [ ] Sur un **ordinateur ancien**, essayer de savoir s'il s'agit d'un
processeur **32 bits ou 64 bits**. Et vérifier qu'il dispose d'au moins 4 Go de
**mémoire vivre** (RAM), sauf pour une distribution spécifiquement conçue pour 
les « ordinosaures » et une utilisation limitée à des logiciels peu gourmands.

- [ ] **De manière facultative**, l'idéal est de disposer d'un **disque dur 
vierge** sur lequel installer le nouveau système d'exploitation. 
En particulier, choisir **un modèle SSD** (même d'une capacité modeste, Linux 
est relativement peu encombrant) **améliore sensiblement les performances**. 
Penser à vérifier la compatibilité avec son matériel.

      De même, l'ajout de mémoire vive (RAM) est souvent une bonne idée en 
termes de performances.

- [ ] Nous déconseillons plutôt cette solution, mais dans le cas où 
l'**on désire conserver Windows** et fonctionner en 
*double boot* (envisageable, mais plus délicat encore, avec MacOS) :
    - [ ] Tâcher de **savoir si le disque est chiffré** (« crypté »). Dans ce 
cas, l'installation risque de s'avérer impossible sous cette forme.
    - [ ] **Défragmenter** les disques sous Windows, surtout le disque système,
en général `C:`.

