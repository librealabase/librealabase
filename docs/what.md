# Qu'est-ce qu'un logiciel libre ?

## Concepts techniques

!!! info "Note"
    Cette partie n'est pas incontournable dans l'absolu, mais la compréhension 
    des termes qui suivent permet de mieux s'approprier la notion de logiciel 
    libre.

### Code source
Pour construire un logiciel, les développeurs écrivent du "**code source**" 
dans un langage de programmation compréhensible par des humains comme eux. 
Ce dernier, une fois transformé en un "**exécutable**" écrit en "langage 
machine" compréhensible par l'ordinateur (au sens large, les smartphones, 
tablettes et objets connectés en font partie), devient illisible pour un 
humain. 

C'est ce dernier que contiennent bien souvent les fichiers fournis à 
l'utilisateur (extensions `.exe`, `.dmg`...) On peut faire l'analogie avec un 
gâteau&nbsp;: c'est lui qu'on nous vend (l'exécutable), sans que cela ne 
permette de retrouver aisément la recette de départ (code source) et ses 
ingrédients. Si l'on veut étudier, vérifier, adapter le logiciel, il faut 
disposer de son code source. Si on y a accès librement, on dit qu'il est 
**_open source_**. Si ce terme est souvent utilisé abusivement comme synonyme 
de **logiciel libre**, il n'implique pas toujours, contrairement à ce dernier, 
l'autorisation de faire ce que l'on souhaite de ce code source. En tout cas,
ce terme met davantage en évidence les libertés offertes à l'utilisateur...

### Système d'exploitation (*OS*)
Cet **_Operating System_** est le programme qui sert de chef d'orchestre à la 
machine, d'interface entre le matériel et les autres logiciels. Sur un 
ordinateur de bureau ou portable, on trouve le plus souvent **Windows, ou 
MacOS (OS X), qui ne sont pas libres**. Mais on peut aussi y installer un OS 
libre comme **Linux** (GNU/Linux en toute rigueur) ou **FreeBSD**. 
C'est la même chose sur les serveurs, où les systèmes Linux ou BSD sont 
d'ailleurs majoritaire dans le monde, contrairement à nos machines 
personnelles.

Sur smartphone, les principaux OS sont **iOS et Android**. Le cœur de ce 
dernier est un logiciel libre, mais les surcouches ajoutées rendent l'ensemble 
opaque, le verrouille et l'on peut constater que 
**la collecte passive de données est massive**. 
Des **variantes d'Android libres** existent, comme **LineageOS** ou **/e/OS** 
qui en dérive et qui permet de n'utiliser aucun des services de Google. Les 
bénéfices de leur liberté peuvent cependant être compromis par l'installation 
d'applications non libres, d'autant que cet OS ne favorise pas l'indépendance 
entre les applications, du moins dans ces anciennes versions. Il convient donc 
de rester vigilant.

iOS est non seulement "propriétaire" (le contraire du logiciel "libre"), mais 
son environnement est conçu pour maintenir captif son utilisateur (achat 
d'applications, musiques et autres appareils de la marque), ce qui respecte 
bien peu sa liberté de choix. En contrepartie, contrairement à Google (derrière 
Android) qui vit principalement du commerce des données, le modèle économique 
d'Apple (fournisseur d'iOS), ses choix stratégiques et l'organisation d'iOS qui 
isole davantage les applis dans des espaces étanches le rendent plutôt plus 
respectueux de la vie privée. 

Notons que pour garantir *complètement* les bienfaits liés aux logiciels libres 
(sécurité, respect de la vie privée notamment), il est indispensable 
d'**utiliser un OS libre**. 
Cependant, utiliser des logiciels libres sur un OS propriétaire est 
déjà un premier pas dans la bonne direction et facilitera une éventuelle future 
transition vers un OS libre, car dans bien des cas, les logiciels libres pour 
Windows ou MacOS existent sous Linux (la réciproque n'étant pas toujours vraie).

### Licence d'utilisation
Un logiciel est fourni avec sa **licence d'utilisation** (ainsi que d'autres 
contenus comme des images, des vidéos)&nbsp;: c'est le texte juridique qui 
indique ce que l'on peut en faire, ou pas.

## Logiciels libres

### Définition
On parle de **licence libre** quand les **quatre "libertés fondamentales"** 
sont garanties pour l'utilisateur (au sens communément utilisé de la 
*Free Software Foundation* et du projet GNU, voir 
[ici](https://www.gnu.org/philosophy/free-sw.html) pour les détails)&nbsp;: 

* liberté d'**utiliser le programme comme on le souhaite**, y compris dans un 
cadre commercial ("liberté 0"),

* liberté d'**étudier son fonctionnement** et de **le modifier** ("liberté 1"), 

* liberté d'en **redistribuer des copies** ("liberté 2") et 

* liberté de faire **de même avec les versions modifiées** ("liberté 3"). 

Un **logiciel** est **libre** s'il dispose d'une licence libre. Dans le cas
contraire, on parle de **logiciel propriétaire** voire de 
**logiciel privateur**.

Cette notion s'étend dans une certaine mesure à diverses créations, qu'elles
soient artistiques ou qu'il s'agisse de documentation par exemple.

!!! danger "Remarque fondamentale"
    Dans tous les cas, il est important de noter qu'**un logiciel gratuit n'est 
    pas un systématiquement un logiciel libre**.



??? info "Un petit complément sur le Copyleft..."

    *Ce concept n'est pas indispensable dans un premier temps...*

    Certaines licences (comme la GPL) sont avec *copyleft*, elles interdisent 
    l'ajout ultérieur de restrictions en interdisant par exemple l'inclusion du 
    logiciel dans un ensemble donnant moins de liberté à l'utilisateur. 

    D'autres au contraire (comme la LGPL) ne 
    sont pas *copyleftées* pour faciliter leur diffusion (notamment concernant des 
    bibliothèques, briques logicielles réutilisables dans de nombreux programmes).

### Exemples de licences libres

**Pour les logiciels**, les différentes versions de la GPL 
(*GNU Public Licence*), Artistic Licence 2.0, BSD dite modifiée, CeCILL 2.1, 
MPL 2.0 (*Mozilla Public Licence*), WTFPL (*What The F\*\*\* Public Licence*,
très permissive), ou le domaine public sont libres. 

Les suivantes également, mais elles ne sont plus totalement compatibles avec 
la GPL&nbsp;: Apache, CeCILL-B et CeCILL-C, MPL 1.1. 

**Pour les documents** et autres contenus que du code
informatique, les licences *Creative Commons* qui
n'incluent pas de restriction comme NC (usage non commercial) ou ND 
(pas de modification) sont libres, tout comme la licence "art libre".

### Licences non libres

La licence de JSON par exemple stipule que « le logiciel sera 
utilisé pour le Bien, non pour le Mal ». La liberté 0 n'est donc pas respectée,
elle n'est donc pas libre. Il en va de même pour tous les *Freeware* (ou 
graticiels) qui n'utilisent pas explicitement une licence libre.
 
Du côté des contenus, c'est pareil pour ceux sous licence **_Creative Commons_ 
avec des clauses restrictives NC ou ND**. 

Vous trouverez de nombreux détails et explications sur 
[gnu.org](https://www.gnu.org/licenses/license-list.html)
