# Décharge de responsabilité

!!! info "Note"
    Nous faisons bien entendu de notre mieux et avons réalisé de nombreuses
    installations sans encombre, mais ce type d'opération présente tout de même
    certains risques, tant au niveau du matériel que des logiciels et de vos
    données. Vous comprendrez donc certainement que nous vous 
    demanderons de signer la décharge ci-dessous pour éviter tout malentendu.


```
Je soussigné    
reconnais avoir été informé(e) des risques inhérents à l'intervention sur 
mon matériel, notamment en ce qui concerne l'installation d'une distribution
GNU/Linux, ainsi que de la nécessité de sauvegarder le contenu de mon 
ordinateur au préalable, en particulier les données et documents personnels,
sur un support indépendant du matériel concerné.

Je renonce par avance à toute poursuite envers les personnes qui procèdent à 
cette intervention ainsi que la ou les associations concernées.

Le     /    / 

Signature :




```
