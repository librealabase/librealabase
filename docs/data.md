# Quelques données peu réjouissantes

[↰](/why)

## Impacts environnementaux et humains[^1]

[^1]:
    Sources&nbsp;: voir [ici](https://www.europe1.fr/technologies/trois-chiffres-pour-comprendre-limmense-impact-ecologique-du-numerique-4036952), [là](https://www.archimag.com/demat-cloud/2019/11/20/ecologie-numerique-chiffres-infographie-conseils-dematerialisation-verte), [là](http://theshiftproject.org/article/climat-insoutenable-usage-video/), [là](https://sante.journaldesfemmes.fr/fiches-sante-du-quotidien/2773685-pollution-numerique-definition-chiffres-solutions-mail-reduire/) ou [là](https://www.grizzlead.com/lincroyable-impact-de-la-pollution-numerique-et-les-bonnes-pratiques-a-adopter-tres-vite/#note11) notamment, en plus des guides mentionnés précédemments... 


* **Pour extraire le cobalt** indispensable notamment pour les accumulateurs des smartphones, en RDC (70% au niveau mondial), environ **250&nbsp;000 congolais, dont de nombreux enfants**, travaillent la roche avec au mieux des outils rudimentaires et dans des conditions inhumaines (voir le reportage sur [swissinfo.ch](https://www.swissinfo.ch/fre/politique/mati%C3%A8res-premi%C3%A8res_le-travail-des-enfants-dans-les-mines-de-cobalt-embarrasse-les-exploitants-suisses/45718332) notamment)

* Le [guide de l'ADEME](https://librairie.ademe.fr/cadic/1866/guide-pratique-impacts-smartphone.pdf) sur le smartphone montre que **88% des français changent de smartphone alors qu'il fonctionne encore** (durée de vie moyenne proche de 2 ans), détaille les impacts et donne des pistes pour mieux se comporter.

* [GreenIT](https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf) fournit également un dossier complet détaillant les différents impacts.

* Le **secteur du numérique** est responsable de plus de **4% des émissions de gaz à effet de serre**, 1,5 fois les émissions du secteur de l'aviation civile ou 116 millions de tours du monde en voiture. Et on envisage que cela représente 5,5% des émissions dès 2025.

* **90% de l'énergie consommée par un smartphone est consommée lors de sa fabrication**, c'est de "l'énergie grise". Au niveau de sa destruction, il faut savoir que **70% des déchets électroniques font l'objet d'un trafic illégal**. Le premier facteur impactant est la production des équipements pour les utilisateurs, le second la consommation électrique de ces derniers. 

* **La consommation d'eau et de ressources est aussi due aux trois quart à la phase de construction** des appareils numériques dans leur ensemble.

* **Plus de la moitié des émissions de gaz à effet de serre générés par le secteur numérique sont dues aux *data centers* et aux infrastructures réseau**, ce qui semblerait montrer les limites des seules actions au niveau individuel. Mais il faut noter qu'une moindre consommation entraîne un besoin moins important aux niveaux réseau et stockage : **le streaming vidéo représente 60% du flux des données** sur internet par exemple, dont environ un tiers pour la pornographie.

* Bien que plus performante au niveau énergétique que la 4G, **la 5G devrait entraîner une augmentation de 18 à 45% de l'empreinte carbone** du secteur numérique en France d'ici 2030 (l'effet rebond en est certainement le principal responsable)

* **Les objets connectés passent de 1 milliard (1% des impacts) en 2010 à 48 milliards (18-23% des impacts) en 2025** et plusieurs autres données comme la taille des écrans suivent cette tendance, compensant plus que largement les gains de consommation dûs aux progrès technologiques...

## Vie privée[^2]

[^2]:
Sources&nbsp;: [Enquête du Professeur Douglas C. Schmidt de la Vanderbilt University](https://digitalcontentnext.org/blog/2018/08/21/google-data-collection-research/) reprise notamment sur le site de [/e/OS](https://e.foundation/fr/about-e/)

* Un smartphone sous Android communique passivement des données **90 fois par heure**, pour un volume de **11,6&nbsp;Mo de données par jour**, s'il est actif. En mode "inactif", c'est encore 40 fois par heure, pour 4,4&nbsp;Mo par jour&nbsp;!

* Sous iOS, il envoie passivement des informations **51 fois par heure, pour 5,7&nbsp;Mo de données par jour** (0,7 fois par heure et 0,76&nbsp;Mo en mode inactif).

* Un appareil sous Android avec le navigateur Chrome en arrière plan envoie les **informations de localisation à Google 340 fois par 24 heures**. Gogole croise des données supposément anonymisées d'applications tierces avec d'autres dont il dispose pour ré-associer l'utilisateur avec ces informations.
