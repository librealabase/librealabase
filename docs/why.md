# Pourquoi passer aux logiciels libres, à Linux ?

## Constat

S'il semble clair que l'informatique a permis de précieuses avancées dans 
certains domaines (médecine, gestion des catastrophes, partage de 
connaissances, etc.), il est indéniable que les conséquences de son 
utilisation massive sont largement délétères :

* **l'exploitation humaine**, dont celle d'enfants pour extraire les ressources
  nécessaires et gérer les déchets, 

* **les pollutions** engendrées par la production des appareils (terminaux, 
  infrastructure et serveur), souvent à durée de vie très limitée, par leur 
  destruction et par l'énergie nécessaire à leur utilisation, sans compter la 
  surconsommation entraînée par la publicité indissociable des principales 
  plateformes commerciales,

* **la vie privée bafouée** et **les données privées exploitées** le plus
  souvent à l'insu de 
  l'utilisateur, 

* **les manipulations** des opinions et même des comportements, la 
  **diffusion d'informations biaisées ou de fausses rumeurs** dévastatrices, 
  qui affaiblissent les démocraties,

* **les addictions** (réseaux sociaux, jeux, etc.) et **le temps confisqué** au 
  détriment d'autres activités essentielles,

* **La fracture numérique** qui isole celles et ceux qui ne peuvent pas 
  matériellement accéder à ces ressources, ou ne savent pas comment...

Les **logiciels libres** ne sont pas une panacée, ils ne résoudront pas tous
ces problèmes. Mais **ils sont incontournables** pour y parvenir.

??? note "Remarque"
    La force des solutions les plus répandues est souvent leur facilité 
    d'utilisation et leur gratuité. Et tout changement implique un temps 
    d'adaptation et quelques efforts.     
    Il est donc bon de réaliser qu'aucune 
    solution «&nbsp;clé en main&nbsp;» ne permettra de résoudre les problèmes 
    précédents sans rien changer par ailleurs. Sans s'en faire nécessairement
    une montagne, puisqu'il existe de nombreuses manière d'améliorer notre
    utilisation du numérique.

    Et la liberté, le respect de la vie humaine et de l'environnement dans 
    lequel nous vivons ne méritent-ils pas quelques efforts&nbsp;? Si vous 
    venez à nos rencontres, vous n'êtes sans doute plus à convaincre. 
    Et nous sommes là pour vous aider dans cette transition&nbsp;!

## Avantages du libre

* Les logiciels libres peuvent se paramétrer plus finement, s'adaptent aux 
  utilisateurs, plutôt que chercher à les piéger et les faire consommer à 
  tout prix. Ainsi, il est possible de rebooster des appareils un peu 
  anciens, ou de prolonger la vie de ceux qu'on possède déjà. C'est bien pour
  **lutter contre l'obsolescence programmée**.

* L'accès au code source permet une **bonne garantie du respect de la vie 
  privée** (si nous ne savons pas le vérifier, d'autres s'en chargent). De 
  même, si le contenu proposé via un logiciel libre était biaisé, orienté 
  suivant l'utilisateur, ce serait visible et on aurait la possiblité de 
  développer ou faire développer une variante qui n'a pas cet inconvénient. 
  Cela **protège également contre les manipulations diverses**.

* Comme ça n'est pas directement l'accès au logiciel qui est à vendre, il n'y
  a **plus d'intérêt à rendre l'utilisateur accro à son utilisation**. Ça ne 
  résout pas tous les problèmes d'addiction, mais on sort d'un modèle 
  intrinsèquement vicieux. 

* L'esprit d'entraide qui prévaut dans le monde du libre et l'accès facilité
  a des outils peu onnéreux (matériel + logiciels) **facilite la lutte contre
  la fracture numérique**.

Voici quelques ressources pour découvrir plus en détail des logiciels libres
et leurs avantages.

* [Framastart.org](https://framastart.org/) permet d'aller rapidement à 
l'essentiel

* Le [site de Stéphane Poinsart](https://ics.utc.fr/libre/site/co/about.html) 
de l'UTC est plus complet, mais reste facile à lire.

* Celui de l'[April](https://www.april.org/articles/divers/intro_ll.html) reste 
une référence.

* D'autres, [ici](https://www.jdbonjour.ch/logiciel-libre/) ou 
[là](https://framablog.org/2020/04/13/juste-un-autre-article-sur-les-licences-libres/) 
permettent de compléter et [Veni, vidi, libli](http://vvlibri.org/fr) est 
particulièrement complet.

* Avantages pour les petites entreprises, sur un 
  [site gouvernemental](https://www.francenum.gouv.fr/guides-et-conseils/pilotage-de-lentreprise/logiciels-de-gestion-de-lentreprise/quels-sont-les)
