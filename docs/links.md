# Sites intéressants

!!! info ":wrench: Partie à compléter, à réorganiser :hammer: Merci pour votre compréhension !"


## Équivalents en logiciels libres

* [FramaLibre](https://framalibre.org/), l'annuaire de Framasoft.

* Le [Comptoir du Libre](https://comptoir-du-libre.org/fr/).

* Sur [Wikipedia](https://fr.wikipedia.org/wiki/Liste_de_logiciels_libres).

* [DistroWatch](https://distrowatch.com/) pour retrouver toutes les 
  distributions GNU/Linux, BSD et autres.

## Entreprises et associations

### Associations de service

* Collectif des [Chatons](https://www.chatons.org/) qui installe des services 
  libres décentralisés. Beaucoup connaissent déjà Framasoft... 

* [Framasoft](https://framasoft.org/) a donné l'exemple et développé ou 
  amélioré pas mal d'outils utilisés dans ce cadre.

### Pas loin de la Roche-sur-Foron

* [AGU3L](https://agu3l.org/) le Groupe d'Utilisateurs de Linux et de Logiciels Libres annécien.

* [Leman Libre](https://leman-libre.org/) dont un membre est venu nous rencontrer régulièrement à la BASE.

* [FabLac](https://fablac.fr/), un fablab associatif local.

### Entreprises engagées
* [MobiCoop](https://www.mobicoop.fr/) pour le covoiturage libre coopératif et 
sans commission.

* [e.foundation](https://e.foundation/) qui a ammélioré 
  [LineageOS](https://lineageos.org/) pour en faire un Androïd libre et 
  dégooglisé nommé **/e/OS**.

* [CozyCloud](https://cozy.io/fr/) le français et 
  [Infomaniak](https://www.infomaniak.com/fr) né et encore implanté à Genève 
  fournissent des services cloud en respectant nos données (avec une petite 
  offre gratuite, puis d'autres plus conséquentes payantes), etc.

* Collectif FairTech (*détails [plus bas](#fairtech)*).

* Voir aussi les annuaires d'[entreprises](https://framalibre.org/annuaires/entreprises) 
  et [associations](https://framalibre.org/annuaires/initiatives) qui 
  promeuvent le libre.

* [Collectif Emmabuntus](https://emmabuntus.org/), leur distribution Linux et 
  leur "clé de réemploi" pour faciliter le clonage de PC en nombre restaurés 
  sous Linux.

### Actualité et hacktivisme
* [LinuxFr](https://linuxfr.org/) pour rester informé.

* Pourquoi le militantisme n'est 
  [pas du lobbyisme](https://ics.utc.fr/libre/site/co/activisme.html)

* [La Quadrature du Net](https://www.laquadrature.net/)

* [S.I.Lex](https://scinfolex.com/quisuisje/)

## Par thème

### Smartphones

* <span id="fairtech"></span>Le collectif [FairTec](https://fairtec.io/fr/) 
    inclut 
    * [FairPhone](https://www.fairphone.com/fr/) (smartphone le plus éthique 
      possible), 
    * [TeleCoop](https://telecoop.fr/) (abonnement mobile poussant à la 
      sobriété numérique), 
    * [Commown](https://commown.coop/) (pour louer ses terminaux plutôt que 
      les acheter et les faire durer au maximum), 
    * [/e/OS-Murena](https://e.foundation/fr/) (Android deGooglisé).

### *Cloud*, outils en ligne
Cette image du *nuage* est largement trompeuse, car ces services consomment 
énormément d'énergie et de ressources. Et si l'on ne peut ou ne souhaite pas
s'en passer, on peut au moins leur préférer des alternatives qui respectent 
l'utilisateur et sa vie privée (et se préoccupent souvent aussi de limiter 
leurs effets environnementaux).

* L'incontournable Framasoft nous parle de 
  [PeerTube](https://framablog.org/2020/10/29/message-aux-youtubeurs-youtubeuses-et-surtout-a-celles-et-ceux-qui-aiment-leurs-contenus/).

* [CozyCloud](https://cozy.io/fr/) est un service français, respectueux des 
  données personnelles, avec une offre de base gratuite.

* [Infomaniak](https://www.infomaniak.com/fr/kdrive) propose des services 
  divers (drive, email...). Il est basé à Genève et offre
  [une formule gratuite](https://www.infomaniak.com/fr/cloud-gratuit).     
  [LaTele.ch](https://latele.ch/emissions/libre-acces/libre-acces-s-2022-e-1?twclid=24zo41wm0h9duj76djzi6iprmi) 
  nous présente cette société qui affiche de beaux principes.

* [La Digitale](https://ladigitale.dev/), un service à explorer pour le monde 
  de l'éducation, tout comme [le service `apps`](https://apps.education.fr/).

### Réseaux sociaux alternatifs

Les alternatives, atomes du [Fediverse](https://fr.wikipedia.org/wiki/Fediverse), ont pour atout la décentralisation, qui garantit davantage de sécurité pour les données personnelles et une bonne indépendance des contenus proposés (*à développer...*)

Exemples d'alternatives.
* [Mastodon](https://mastodon.social/about)
  
* [Diaspora*](https://diasporafoundation.org/)

* Etc., voir [https://the-federation.info/](https://the-federation.info/) en attendant davantage de détails...


### Sobriété, *(s)low tech*

* [Forum Grenoblois LowTRE](https://forum-lowtre-ecosesa.univ-grenoble-alpes.fr/) 
  autour du low-tech dans la recherche et l'enseignement.

* [Jerry Do-It-Together](https://fr.wikipedia.org/wiki/Jerry_Do-It-Together): 
  projet bidon&nbsp;?

* [L'atelier paysan](https://www.latelierpaysan.org/) pour retrouver de 
  l'autonomie.

### Utilitaires, astuces

* [Clavier fr-oss](https://lehollandaisvolant.net/?d=2020/09/24/19/21/46-fr-oss-sous-windows-sans-installer-quoi-que-ce-soit) et [autres dispositions](http://accentuez.mon.nom.free.fr/Clavier-PKL.php) en version portable pour Windows (le second permet d'avoir différentes dispositions sous la main dont [Bépo](http://bepo.fr/wiki/Accueil), on peut utiliser avec le layout fr-oss issu du premier).
* 
* [Herberger sa propre instance PeerTube ?](https://www.jonathandupre.fr/articles/24-logiciel-scripts/217-installation-d-une-instance-peertube-sur-debian-stretch-9-6/)
  
* https://www.zdnet.fr/blogs/l-esprit-libre/education-nationale-12-projets-numeriques-libres-39939967.htm

* [DavX](https://tutox.fr/2022/04/14/synchroniser-lagenda-de-son-smartphone-sans-utiliser-google/) pour synchroniser son agenda de smartphone (ou PC) sans Google.

* [OpenCliparts](https://openclipart.org/share)

* [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page?uselang=fr)

* [Des libres en communS](https://deslivresencommuns.org/post/presentation/), à nouveau par Framasoft.

* [JustGetMyData](https://justgetmydata.com/) pour faciliter la récupération de ses données perso. détenues par différents sites privateurs.

## Culture du libre

### Écrits fondateurs, lectures
* La cathédrale ou le bazar", en [version originale anglaise](http://www.laputan.org/pub/papers/Cathedral-Paper.pdf) ou [traduite en français](https://archive.framalibre.org/IMG/cathedrale-bazar.pdf)
* [Code is Law](https://framablog.org/2010/05/22/code-is-law-lessig/) (2000, Lawrence Lessig, traduit en français).
* [Le livret du libre](https://framagit.org/livretdulibre/livretdulibre/raw/master/livret_libre.pdf) de 2014 reste pertinent.
* [Le Guide d'Autodéfense Numérique](https://guide.boum.org/news/quatrieme_edition/), complet (bien qu'un peu daté) mais volumineux.
* [Sortir de FaceBook](https://sortirdefacebook.wordpress.com/) ou des autres réseaux GAFAMisés...
* [Sur la notion de "Communs"](https://lescommuns.org/)
* [Un MOOC](https://mooc.chatons.org/) sur les [chatons](https://www.chatons.org/) parce qu'il n'y a pas que Framasoft qui deGooglise...
* https://ladigitale.dev/blog/un-outil-numerique-educatif-responsable-c-est-quoi

### Vidéos
* [Shoshana Zuboff](https://peertube.pi2.dev/videos/watch/434fe40c-9458-433b-9e84-d12ccea3d160) et son concept de capitalisme de surveillance.
* [Nothing to Hide](https://peertube.fr/w/4f057fb6-5491-4f23-822b-12e6c4884447)&nbsp;: rien à cacher&nbsp;? Rien n'est moins sûr...
* [Un autre smartphone est-il possible](https://tube.ac-lyon.fr/w/bzSKjffUHSrJTmDd7CYcPa) où il s'agira notamment du FairPhone.


