## Libre Install Party Linux

* Discussions, **échanges, conseils** ; **présentation** des logiciels libres, 
de Linux.

* **Mise à disposition d'ordinateurs sous Linux**, pour voir si ça vous 
convient ou vous aider à prendre en main ce système d'exploitation.

* **Installation** à nos côtés de distribution GNU/Linux **sur des ordinateurs 
qui ne craignent rien**, pour découvrir ou s'entraîner sans aucun risque.

* **Préparation de clé USB**
    - pour installer Linux chez vous ensuite
    - **pour tester Linux sans rien installer**, ou disposer d'un système de 
secours, ou pour une utilisation spécifique ponctuelle (sauvegardes, 
test de matériel, récupération de fichiers sur un système vérolé, etc.)
    - pour expérimenter et comparer, sans installation, différentes 
distributions GNU/Linux.
    - **pour disposer de logiciels libres « portables »**, utilisables sans 
installation sur n'importe quel ordinateur **sous Windows**.

* Installation d'une **machine virtuelle en Linux sans modifier votre 
système d'exploitation actuel** (Windows ou MacOS), par exemple avec
Virtualbox.

* Et bien sûr, **installation sur votre ordinateur d'une distribution
GNU/Linux** si tel est votre choix (nous pourrons analyser vos besoin au 
préalable pour le dééterminer).

* Expérimentation d'installation plus inhabituelle : distributions spécialisées
(*axée sécurité, anonymat, multimédia, jeux*), BSD, etc.

* **Reconditionnement d'anciens ordinateurs** réinstallés à neuf sous 
GNU/Linux. Ils peuvent être vendus à prix libre ou donnés à des personnes dans 
le besoin, à des associations.

## À venir ?

!!! help "Pistes de projets complémentaires"

    Nous pourrions envisager d'autres actions, *a priori* plus ponctuelles. 
    N'hésitez pas à venir nous en parler. Et encore moins à nous rejoindre
    si vous avez des compétences dans un domaine donné :wink:.

    * Élargissement de nos réflexions et des services proposés :
      **services en ligne**, **réseaux sociaux** alternatifs, 
      **téléphonie** (ex.
      [/e/OS, un Android degooglisé](https://e.foundation/fr/)).

    * **Événements thématiques** : Linux et les jeux, le multimédia, 
      la domotique, etc. 

    * **Conférences** de spécialistes du domaine.

    * **Soirée débat**, avec projection d'un **film** (ou pas).
