## Rencontres mensuelles

Retrouvons-nous tous 

* le **troisième samedi du mois**, de **9h à 12h** 
  (**15/03/2025**, 19/04, 17/05, 21/06...)
  
* à la **Maison pour Tous d'Amancy**, salle Bramafan. Voir sur
[OpenStreetMap](https://www.openstreetmap.org/?mlat=46.074716&mlon=6.328568#map=16/46.07472/6.32858)
si besoin :

<center>![QR code vers la carte OSM](img/qr_amancy.png)</center>

Vous pouvez télécharger un petit [flyer](assets/Rencontres_LL.pdf)
qui reprend l'essentiel (lieu, horaire, quelques activités proposées).

!!! info "Tips"
    On se gare sans souci sur place, y compris à vélo juste devant l'entrée et la
    gare de la Roche-sur-Foron est moins de 3 km.

    N'hésitez pas à nous contacter avant, en exposant vos attentes et projets,
    nous n'en serons que plus efficaces le jour J.

    Et nos rencontres sont sous le signe de la bonne humeur, donc les boissons
    ou douceurs à partager sont les bienvenues !

## Inscription à la liste d'échange et d'entraide

Histoire de ne pas rester démunis face aux difficultés éventuelles,
ou pour faire circuler quelques infos importantes, nous vous proposons
une **liste d'échange et d'entraide par email**, à laquelle 
**[vous pouvez vous inscrire ici](https://framalistes.org/sympa/info/libre_a_la_base)**). 

Vous pourrez y poster vos questions et les autres membres essaieront sans 
doute d'y répondre, faisant ainsi vivre l'esprit d'entraide inhérent 
au logiciel libre. :+1:

*N'ayez aucune crainte, la désinscription est immédiate et ne pose aucune 
difficulté si vous deviez changiez d'avis.*
