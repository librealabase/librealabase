# Comment « passer aux logiciels libres » ?

## J'hésite encore...

Sans être très à l'aise avec l'informatique, sans vouloir trop changer ses
habitudes, on peut déjà opérer une transition en douceur. 

> Nous ne détaillons pas ici *comment* le faire, l'idéal est de venir en parler 
  avec nous sur place si besoin...

* Commencer par privilégier des **formats ouverts** (on explique ce concept
  [ici](/formats)) quand on enregistre ses 
  fichiers, plus facilement intéropérables c'est-à-dire utilisable par divers
  logiciels et pas uniquement par une unique appli qui vous enferme dans son
  écosystème fermé et fait de vous un client captif (le problème reste valable 
  pour les logiciels gratuits non libres).

* En restant **sous son sytème d'exploitation habituel** (souvent Windows ou 
  MacOS), **privilégier l'utilisation de logiciels libres**. 
  Et pour en disposer partout quand on est un peu nomade, 
  l'idéal d'**en conserver une version « portable » sur clé USB**.

!!! note "Quelques sites pour télécharger de tels logiciels"
    [FramaKey](https://framakey.org/), voire
    [PortableAppels.com](https://portableapps.com/fr), ou 
    [LiberKey.com](https://www.liberkey.com/fr.html) mais en vérifiant alors 
    que les logiciels choisis sont bien libres, ce qui n'est pas toujours le 
    cas. 

* **Prendre la peine de contribuer au développement des logiciels libres** que
l'on apprécie. Il suffit de prendre le temps de **signaler un _bug_** ou un 
souhait d'amélioration aux auteurs, de **s'entraider entre utilisateurs**, de
promouvoir les applis libres que l'on apprécie, mais on peut aussi 
**participer à une traduction**, à la rédaction de **documentation**, proposer 
des **graphismes**, etc. Et bien évidemment, **faire un don** aux développeurs
quand il s'agit de bénévoles ou de professionnels indépendants.

* Si vous désirez aller plus loin, mais simplement découvrir, ou n'utiliser que
ponctuellement Linux, **une distribution Linux « live » sur clé USB** (sans 
installation sur l'ordinateur), ou l'utilisation d'**une machine virtuelle**
(un programme qui tourne sur votre ordinateur, à l'intérieur duquel un PC sous
Linux est simulé) **vous permettent de ne prendre aucun risque** et de ne pas 
avoir à changer vos habitudes le reste du temps.

## OK, je passe à Linux

Pour « sauter le pas » et travailler avec un véritable système d'exploitation
GNU/Linux installé, après avoir validé ensemble que cela répondra bien à vos 
attentes, différentes possibilités s'offrent à 
vous :

* Travailler **en _double boot_**, à savoir que vous choisirez au démarrage de
l'ordinateur si vous voulez travailler sous Linux, ou sous l'ancien sytème
que vous aurez conservé. *Cela permet de tester dans la durée sans prendre de 
risque, mais il faut avouer que le fait d'avoir à redémarrer pour passer
d'un système à l'autre peut devenir rapidement agaçant*.

* **N'installer que GNU/Linux** sur votre machine. En cas d'hésitation, ce peut
être un second ordinateur, idéalement un ancien PC reconditionné pour lui 
donner une seconde vie. Nous en proposons parfois à la BASE, à prix libre.

* **Il arrive que l'on ai encore besoin d'*un* logiciel qui ne fonctionne que
sous Windows** par exemple. Il est parfois possible de contourner le problème 
en utlisant un sorte d'émulation de Windows, mais c'est rarement idéal et 
parfois impossible. Une solution peut être d'installer 
**Windows dans une machine virtuelle**, que vous lancez depuis votre PC sous 
Linux uniquement quand vous aurez besoin de cette application récalcitrante.

!!! info "IMPORTANT"
    **_Quoi qu'il en soit, avant d'installer une distribution Linux, prenez le
    temps de préparer votre ordinateur en suivant [notre guide](/preinstall)
    SVP._**

## J'en veux davantage !

> **_La route est longue, mais la voie est libre !_** 
> (Slogan de l'association [Framasoft](https://framasoft.org))

Utiliser des outils numériques qui n'aliènent pas leurs utlisateurs ne 
s'arrête pas à l'ordinateur individuel. Les **outils en ligne** (*cloud*, 
etc.), les **ordiphones** (ou *smartphones*), les **réseaux sociaux** sont 
autant d'environnements dans lesquels il est possible de regagner nos libertés.

Si nous ne proposons guère de ressources pour l'instant, nous sommes motivés 
pour **explorer ensemble ces domaines**.

!!! note "En attendant..."
    Quelques pistes [concernant les smartphones](https://linuxfr.org/news/les-10-paliers-de-liberation-d-un-telephone-android).
    En particulier, [e/OS/](https://e.foundation/fr/) propose déjà des 
    solutions concrètes intéressantes.
    

## Où trouver des logiciels et ressources libres
* [Wikipedia](https://fr.wikipedia.org/wiki/Correspondance_entre_logiciels_libres_et_logiciels_propri%C3%A9taires) 
fournit une liste de correspondances entre logiciels libres et propriétaires, 
pour faciliter la transition.

* L'annuaire [FramaLibre](https://framalibre.org/) des logiciels libres de 
[Framasoft](https://framasoft.org/fr/full), ou leur [FramaKey](https://framakey.org/) 
fournissant des logiciels libres pour Windows 
[en toute simplicité](https://docs.framasoft.org/fr/framakey/utiliser.html#prise-en-main). 
Ou leur page indiquant les [alternatives](https://framalibre.org/alternatives) 
aux logiciels propriétaires classiques (ou [ici](https://degooglisons-internet.org/fr/alternatives/) 
dans une version moins à jour mais ergonomique, ou 
[là](https://degooglisons-internet.org/fr/list/) idem pour les services en 
ligne).

* Le [site de l'April](https://www.april.org/Catalogue_Libre) présente 26 
logiciels libres courants, 
[Clubic](https://www.clubic.com/telecharger/actus-logiciels/article-842984-1-passez-libre-selection-logiciels-libres-open-source.html) 
présente également un bon nombre parmi les plus connus, mais rendez-vous sur 
leurs sites d'origine plutôt que de les télécharger depuis ce site.
  
* [GoFOSS](https://gofoss.net/fr/) (FOSS pour *Free OpenSource Software*) 
s'intéresse aussi aux téléphones mobiles et services cloud.
