# librealabase

*Site ressource de nos « Rencontres Logiciels Libres » (troisième samedi du mois, 9h-12h / Maison pour Tous - 74800 AMANCY)*

Le site est base sur MkDocs/Material. Les pages sont donc éditées en MarkDown, stockées dans le répertoire `docs/`. Voir [ici](https://www.mkdocs.org/getting-started/) pour les détails techniques et l'installation si l'on veut contribuer autrement qu'en utilisant l'éditeur intégré de GitLab.

* Chez soi, on peut visualiser le résultat en exécutant `mkdocs serve`.

* Le site est automatiquement mis à jour après un court délai, quand on *push* sur le dépôt GitLab.
