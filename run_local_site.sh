#!/usr/bin/env bash

echo Get into .venv
# Doesn't work on FAT...
python3 -m venv .venv
source .venv/bin/activate

# Only needed 1st time ?
echo Install modules
python3 -m pip install -r requirements.txt

#echo Make docs index
#./list_files.sh

echo Go !
browse http://127.0.0.1:8000/ &
mkdocs serve

echo Bye, get out of .venv...
deactivate
